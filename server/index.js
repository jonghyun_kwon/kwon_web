const Hapi = require('@hapi/hapi');
const loan = require('./pages/loanApplyBankListMock');

const init = async () => {
  const server = Hapi.server({
    port: 9100,
    host: 'localhost',
  });

  await server.register(loan);

  server.route({
    method: 'GET',
    path: '/api',
    handler: () => ('Hello World!'),
  });

  await server.start();
  console.log('Server running on %s', server.info.uri);
};

process.on('unhandledRejection', (err) => {
  console.log(err);
  process.exit(1);
});

init();
