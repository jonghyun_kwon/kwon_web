const mockData = () => {
  const dataSource = [];
  for (let i = 0; i < 200; i += 1) {
    dataSource.push({
      key: i,
      name: `KWON ${i}`,
      age: 32 + i,
      address: `${i}10 Downing Street`,
    });
  }
  return dataSource;
};

const dataSource = mockData();

// eslint-disable-next-line no-return-await
const sleep = async (value, time) => await new Promise(resolve => {
  setTimeout(() => (resolve(value)),
    time);
});

exports.plugin = {
  name: 'loanApply',
  version: '1.0.0',
  register: async (server) => {
    // Create a route for example

    server.route({
      method: 'GET',
      path: '/api/loanApply',
      handler: () => sleep(dataSource, 3000),
    });

    server.route({
      method: 'PUT',
      path: '/api/loanApply/{key}',
      handler: (request) => {
        dataSource[request.params.key] = request.payload;
        return sleep(dataSource, 3000);
      },
    });
  },
};
