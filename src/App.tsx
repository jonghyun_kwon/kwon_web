import 'assets/styles/main.less';
import 'assets/styles/tailwind.sss';
import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter, Route } from 'react-router-dom';
import MainLayout from './layout/MainLayout';

declare const document: any;

const Root = () => (
  <div className="App">
    <BrowserRouter>
      <Route path="/" component={MainLayout} />
    </BrowserRouter>
  </div>
);

ReactDOM.render(<Root />, document.getElementById('root'));
