import React, { createContext, useCallback, useContext, useEffect, useState } from 'react';
import axios from 'axios';
import BreadContentPath from 'components/BreadContentPath';
import {
  Button,
  Form,
  Input, message,
  Modal,
  PageHeader,
  Select,
  Spin,
  Table,
} from 'antd';
import DetailRowModal from 'components/DetailRowModal';
import { ModalFunc } from 'antd/es/modal/confirm';
import { ModalFuncProps } from 'antd/es/modal';

const columns = [
  {
    title: 'Name',
    dataIndex: 'name',
    type: 'text',
    key: 'name',
  },
  {
    title: 'Age',
    dataIndex: 'age',
    type: 'number',
    key: 'age',
  },
  {
    title: 'Address',
    dataIndex: 'address',
    type: 'text',
    key: 'address',
  },
];

function LoanApplyBankList() {
  const [pagination, setPage] = useState({ current: 1, pageSize: 10 });
  const [dataSource, setDataSource] = useState(null);
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(null);
  let modal:{ destroy: () => void; update: (newConfig: ModalFuncProps) => void };

  const fetchDataSource = async () => {
    setError(null);
    setDataSource(null);
    setLoading(true);

    const response = await axios.get('/api/loanApply');

    setDataSource(response.data);
    setLoading(false);
  };
  useEffect(() => {
    fetchDataSource();
  }, []);

  const editSubmit = useCallback((value, record) => {
    const messageKey = 'editSubmit';
    const sendData = async () => {
      message.loading({
        content: '내용 업데이트 중...',
        key: messageKey,
        duration: 5,
      });

      const dataIndex = record.key;
      const payload = { ...value, key: record.key };
      const response = await axios.put(`/api/loanApply/${dataIndex}`, payload);

      // alert success;
      message.success({
        content: '업데이트 성공',
        key: messageKey,
        duration: 3,
      });

      fetchDataSource();
      modal.destroy();
    };
    sendData()
  }, []);

  const clickRow = useCallback((event, record) => {
    const option = {
      columns,
      record,
      modal,
      editSubmit,
    };
    modal = Modal.info({
      icon: null,
      title: null,
      width: '800px',
      content: <DetailRowModal option={option} />,
      okText: '취소',
      okType: 'default',
    });
  }, []);

  const onRowContext = useCallback(
    (record, rowIndex) => ({
      onClick: (event) => {
        console.log(record, rowIndex);
        clickRow(event, record);
      }, // click row
      // onDoubleClick: event => {}, // double click row
      // onContextMenu: event => {}, // right button click row
      // onMouseEnter: event => {}, // mouse enter row
      // onMouseLeave: event => {}, // mouse leave row
    }),
    [],
  );

  return (
    <div>
      <div className="mt-0 p-2 pr-6 pl-6 bg-black-200">
        <BreadContentPath />
        <h1 className="mt-1 mb-2">대출 신청</h1>
      </div>

      <div className="mt-6 mr-6 ml-6">
        <Spin spinning={loading}>
          <div className="mb-4 bg-black-200 p-4">
            <Form layout="inline">
              <Form.Item>
                <Select
                  showSearch
                  style={{ width: 200 }}
                  placeholder="검색 필드 선택하세요."
                  optionFilterProp="children"
                  // onChange={onChange}
                  // onFocus={onFocus}
                  // onBlur={onBlur}
                  // onSearch={onSearch}
                  // filterOption={(input, option) =>
                  //     option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
                >
                  <Select.Option value="jack">Jack</Select.Option>
                  <Select.Option value="lucy">Lucy</Select.Option>
                  <Select.Option value="tom">Tom</Select.Option>
                </Select>
              </Form.Item>
              <Form.Item>
                <Input placeholder="검색어를 입력하세요." />
              </Form.Item>
              <Form.Item>
                <Button type="primary">Submit</Button>
              </Form.Item>
            </Form>
          </div>
          <div className="bg-black-200">
            <Table
              dataSource={dataSource}
              columns={columns}
              pagination={pagination}
              onRow={onRowContext}
              rowKey={(record) => record.name}
            />
          </div>
        </Spin>
      </div>
    </div>
  );
}

export default LoanApplyBankList;
