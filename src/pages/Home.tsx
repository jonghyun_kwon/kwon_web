import React from 'react';

function Home() {
  return (
    <div className="max-w-sm mx-auto flex p-6 bg-white rounded-lg shadow-xl">
      <div className="flex-shrink-0">
        <img className="h-12 w-12" src="/img/logo.svg" alt="ChitChat Logo" />
      </div>
      <div className="ml-6 pt-1">
        <h4 className="text-xl text-gray-900 leading-tight">HOME</h4>
        <p className="text-base text-gray-600 leading-normal">
          You have a new message!
        </p>
        <div className="bg-blue-100">blue100</div>
        <div className="bg-blue-300">blue200</div>
        <div className="bg-blue-500">blue500</div>
        <div className="bg-blue-700">blue700</div>
      </div>
    </div>
  );
}

export default Home;
