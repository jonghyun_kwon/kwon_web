import React, { useCallback, useState } from 'react';
import { Avatar, Badge, Layout, Menu, Dropdown } from 'antd';
import {
  MenuUnfoldOutlined,
  MenuFoldOutlined,
  BellOutlined,
  UserOutlined,
} from '@ant-design/icons';

import { Route, Switch } from 'react-router-dom';
import Home from '../pages/Home';
import About from '../pages/About';
import LoanApplyBankList from '../pages/LoanApplyBankList';
import useMenuCollapsedStatus from './useMenuCollapsed';
import { SideMenu } from './SideMenu';

import './MainLayout.sss';

const { Header, Content } = Layout;

function handleButtonClick(e) {
  console.log('click left button', e);
}

function handleMenuClick(e) {
  console.log('click', e);
}

const menu = (
  <Menu onClick={handleMenuClick}>
    <Menu.Item key="1" icon={<UserOutlined />}>
      1st menu item
    </Menu.Item>
    <Menu.Item key="2" icon={<UserOutlined />}>
      2nd menu item
    </Menu.Item>
    <Menu.Item key="3" icon={<UserOutlined />}>
      3rd menu item
    </Menu.Item>
  </Menu>
);

function MainLayout() {
  const [isCollapsed, toggle] = useMenuCollapsedStatus();

  return (
    <Layout className="h-screen">
      <SideMenu collapsed={isCollapsed} />
      <Layout className="h-full">
        <Header className="site-layout" style={{ padding: 0 }}>
          {isCollapsed ? (
            <MenuUnfoldOutlined
              className="trigger"
              onClick={() => toggle()}
            />
          ) : (
            <MenuFoldOutlined
              className="trigger"
              onClick={() => toggle()}
            />
          )}
          <span className="absolute right-10">
            <Badge dot className="align-middle mr-2">
              <Dropdown overlay={menu}>
                <BellOutlined className="text-xl" />
              </Dropdown>
            </Badge>
            <Avatar icon={<UserOutlined />} className="mr-4 ml-4" />
            <span>USER NAME</span>
          </span>
        </Header>
        <Content className="mt-1">
          <div>
            <Switch>
              <Route path="/home" component={Home} />
              <Route path="/about" component={About} />
              <Route
                path="/loan_apply_bank_list"
                component={LoanApplyBankList}
              />
            </Switch>
          </div>
        </Content>
      </Layout>
    </Layout>
  );
}

export default MainLayout;
