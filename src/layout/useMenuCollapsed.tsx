import { useState, useEffect } from 'react';

type returnType = [boolean, () => void];

function useMenuCollapsedStatus(): returnType {
  const [isCollapsed, setIsCollapsed] = useState(false);

  useEffect(() => {
    console.log(`effect:${isCollapsed}`);
  });

  function toggle() {
    console.log(`now isCollapsed : ${isCollapsed}`);
    setIsCollapsed(!isCollapsed);
    console.log(`changed isCollapsed : ${isCollapsed}`);
  }
  return [isCollapsed, toggle];
}

export default useMenuCollapsedStatus;
