import React, { useState } from 'react';

// eslint-disable-next-line @typescript-eslint/no-empty-function
let obj: any = {};
const PageRowUpdateContext = React.createContext([obj, (p: (s) => any) => {}]);

const PageRowUpdateProvider = (props) => {
  const [state, setState] = useState({
    hello: 'hello',
  });
  return (
    <PageRowUpdateContext.Provider value={[state, setState]}>
      {/* eslint-disable-next-line react/destructuring-assignment */}
      {props.children}
    </PageRowUpdateContext.Provider>
  );
}

export { PageRowUpdateContext, PageRowUpdateProvider };
