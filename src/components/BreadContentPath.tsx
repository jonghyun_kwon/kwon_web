import React from 'react';
import { Breadcrumb } from 'antd';

function BreadContentPath() {
  // TODO: 현재 패스로 작업 필요.
  return (
    <Breadcrumb>
      <Breadcrumb.Item>Home</Breadcrumb.Item>
      <Breadcrumb.Item>
        <a href="/aa">Application Center</a>
      </Breadcrumb.Item>
      <Breadcrumb.Item>
        <a href="/bb">Application List</a>
      </Breadcrumb.Item>
      <Breadcrumb.Item>An Application</Breadcrumb.Item>
    </Breadcrumb>
  );
}

export default BreadContentPath;
