/* eslint-disable eqeqeq */
import React, { Dispatch, useCallback, useContext, useEffect, useState } from 'react';
import {
  Button,
  Descriptions,
  Form,
  Input,
  Radio,
  PageHeader,
  Tooltip,
  InputNumber, message,
} from 'antd';
// eslint-disable-next-line import/no-extraneous-dependencies
import { EditOutlined } from '@ant-design/icons';

import axios from 'axios';
import { PageRowUpdateContext } from 'components/PageRowUpdateContext';

export interface DetailRowModalProps {
  option: {
    record: any,
    columns: any[],
    modal: any,
    editSubmit: any
  }
}

export interface Option {
  option: {
    record: any,
    columns: any[],
    modal: any,
    editSubmit: any,
    editMode: Dispatch<boolean>,
  }
}

const layout = {
  labelCol: { span: 3 },
  wrapperCol: { span: 16 },
};

const tailLayout = {
  wrapperCol: { offset: 3, span: 16 },
};

function EditRow(Option: Option) {
  const { editSubmit, record, editMode, columns } = Option.option;

  const messageKey:string = 'update';
  const [form] = Form.useForm();

  const onSubmit = useCallback(() => {
    form
      .validateFields()
      .then((value) => {
        editSubmit(value, record);
      })
      .catch((errorInfo) => {
        console.log(errorInfo);
      });
  }, [record]);

  const onFinish = (values) => {
    console.log(values);
  };

  return (
    <PageHeader
      ghost={false}
      title="대출 신청 상세 수정"
      extra={[
        <Tooltip title="취소" key="cancel">
          <Button
            key="a"
            ghost={false}
            type="text"
            icon={<EditOutlined />}
            onClick={() => editMode(false)}
          />
        </Tooltip>,
      ]}
    >
      <Form
        form={form}
        onFinish={onFinish}
        {...layout}
        initialValues={record}
      >
        {columns.map((column) => (
          <Form.Item
            label={column.title}
            name={column.dataIndex}
            key={column.dataIndex}
            id={column.dataIndex}
          >
            {column.type == 'number' ? (
              // <InputNumber defaultValue={option.record[column.dataIndex]} />
              <InputNumber />
            ) : (
              // <Input defaultValue={option.record[column.dataIndex]} />
              <Input />
            )}
          </Form.Item>
        ))}
        <Form.Item {...tailLayout}>
          {/* <Button type="primary" htmlType="submit">Submit</Button> */}
          <Button type="primary" onClick={onSubmit}>
            Submit
          </Button>
        </Form.Item>
      </Form>
    </PageHeader>
  );
}

function ViewRow(Option: Option) {
  const { editMode, columns, record } = Option.option;
  return (
    <PageHeader
      ghost={false}
      title="대출 신청 상세"
      extra={[
        <Tooltip title="Edit" key="edit">
          <Button
            key="e"
            ghost={false}
            type="text"
            icon={<EditOutlined />}
            onClick={() => editMode(true)}
          />
        </Tooltip>,
      ]}
    >
      <Descriptions layout="horizontal" bordered>
        {columns.map((column) => (
          <Descriptions.Item label={column.title}>
            {record[column.dataIndex]}
          </Descriptions.Item>
        ))}
      </Descriptions>
    </PageHeader>
  );
}

function DetailRowModal(Option: DetailRowModalProps) {
  const [isEdit, setEdit] = useState(false);
  const option = {
    editMode: setEdit,
    columns: Option.option.columns,
    record: Option.option.record,
    modal: Option.option.modal,
    editSubmit: Option.option.editSubmit,
  };

  return (
    <>
      {isEdit ? (
        <EditRow
          option={option}
        />
      ) : (
        <ViewRow
          option={option}
        />
      )}
    </>
  );
}

export default DetailRowModal;
