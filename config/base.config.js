const path = require('path');

const ManifestPlugin = require('webpack-manifest-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
// const ExtractTextPlugin = require('extract-text-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

function recursiveIssuer(m) {
    if (m.issuer) {
        return recursiveIssuer(m.issuer);
    } else if (m.name) {
        return m.name;
    } else {
        return false;
    }
}

module.exports = {
    entry: {
        // 'vendor': ['react', 'react-dom'],
        'app': path.resolve(__dirname, '..', 'src', 'App.tsx'),
        'login': path.resolve(__dirname, '..', 'src', 'Login.tsx')
    },
    optimization: {
        splitChunks: {
            cacheGroups: {
                // vendor: {
                //     chunks: 'initial',
                //     name: 'vendor',
                //     enforce: true,
                // },
                app: {
                    name: 'login',
                    test: (m, c, entry = 'app') =>
                        m.constructor.name === 'CssModule' && recursiveIssuer(m) === entry,
                    chunks: 'initial',
                    enforce: true,
                },
                login: {
                    name: 'login',
                    test: (m, c, entry = 'login') =>
                        m.constructor.name === 'CssModule' && recursiveIssuer(m) === entry,
                    chunks: 'initial',
                    enforce: true,
                },
            },
        },
    },
    output: {
        filename: '[name].[chunkhash].js',
        chunkFilename: '[name].[chunkhash].chunk.js',
    },
    resolve: {
        extensions: ['.js', '.ts', '.tsx'],
        alias: {
            'components': path.resolve(__dirname, '..', 'src', 'components'),
            'containers': path.resolve(__dirname, '..', 'src', 'containers'),
            'assets': path.resolve(__dirname, '..', 'src', 'assets'),
        },
    },
    module: {
        rules: [
            {
                test: /\.(js|jsx)$/,
                exclude: /node_modules/,
                use: [
                    {
                        loader: 'babel-loader',
                    },
                ],
            },
            {
                test: /\.(ts|tsx)$/,
                exclude: /node_modules/,
                use: [
                    {
                        loader: 'ts-loader',
                    },
                ],
            },
            // {
            //     test: /\.css$/,
            //     exclude: /node_modules/,
            //     use: ExtractTextPlugin.extract({
            //         fallback: 'style-loader',
            //         use: 'css-loader',
            //     }),
            // },
            {
                test: /\.scss$/,
                exclude: /node_modules/,
                use: [
                    MiniCssExtractPlugin.loader,
                    // 'style-loader',
                    'css-loader',
                    'sass-loader'
                ],
            },
            {
                test: /\.less$/,
                use: [
                    MiniCssExtractPlugin.loader,
                    // 'style-loader',
                    'css-loader',
                    {
                        loader: 'less-loader',
                        options: {
                            lessOptions: {
                                javascriptEnabled: true,
                            },
                        },
                    }
                ]
            },
            {
                test: /\.(png|svg|jpg|gif)$/,
                use: [
                    'file-loader',
                ],
            },
            {
                test: /\.(woff|woff2|eot|ttf|otf)$/,
                use: [
                    'file-loader',
                ],
            },
            {
                test: /\.sss$/,
                use: [
                    MiniCssExtractPlugin.loader,
                    // 'style-loader',
                    'css-loader',
                    {
                        loader: 'postcss-loader',
                        options: {
                            postcssOptions: {
                                ident: 'postcss',
                                plugins: [
                                    require('tailwindcss'),
                                    require('autoprefixer'),
                                ],
                            },
                        }
                    },
                ]
            }
        ]
    },
    // plugins
    plugins: [
        new MiniCssExtractPlugin({
            filename: '[name].[chunkhash].css',
        }),
        new ManifestPlugin({
            fileName: 'assets.json',
            basePath: '/',
        }),
        new HtmlWebpackPlugin({
            template: './src/index.html',
            filename: './index.html',
            //chunks: ['app', 'vendor']
            chunks: ['app', '[name].[chunkhash].css']
        }),
        new HtmlWebpackPlugin({
            template: './src/login.html',
            filename: './login.html',
            // chunks: ['login', 'vendor']
            chunks: ['login', '[name].[chunkhash].css']
        }),
    ],
};