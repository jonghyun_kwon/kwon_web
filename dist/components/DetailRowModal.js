import React, { useState } from 'react';
import { Button, Descriptions, Form, Input, Radio, PageHeader, Tooltip } from "antd";
import { EditOutlined } from '@ant-design/icons';
function EditRow(Option) {
    return (React.createElement(PageHeader, { ghost: false, title: "\uB300\uCD9C \uC2E0\uCCAD \uC0C1\uC138 \uC218\uC815", extra: [
            React.createElement(Tooltip, { title: "\uCDE8\uC18C", key: "cancel" },
                React.createElement(Button, { key: "a", ghost: false, type: "text", icon: React.createElement(EditOutlined, null), onClick: function () { return Option.edit(false); } })),
        ] },
        React.createElement(Form, { initialValues: { layout: 'horizontal' } },
            React.createElement(Form.Item, { label: "Form Layout", name: "layout" },
                React.createElement(Radio.Group, null,
                    React.createElement(Radio.Button, { value: "horizontal" }, "Horizontal"),
                    React.createElement(Radio.Button, { value: "vertical" }, "Vertical"),
                    React.createElement(Radio.Button, { value: "inline" }, "Inline"))),
            React.createElement(Form.Item, { label: "Field A" },
                React.createElement(Input, { placeholder: "input placeholder" })),
            React.createElement(Form.Item, { label: "Field B" },
                React.createElement(Input, { placeholder: "input placeholder" })),
            React.createElement(Form.Item, null,
                React.createElement(Button, { type: "primary" }, "Submit")))));
}
function ViewRow(Option) {
    return (React.createElement(PageHeader, { ghost: false, title: "\uB300\uCD9C \uC2E0\uCCAD \uC0C1\uC138", extra: [
            React.createElement(Tooltip, { title: "Edit", key: "edit" },
                React.createElement(Button, { key: "e", ghost: false, type: "text", icon: React.createElement(EditOutlined, null), onClick: function () { return Option.edit(true); } })),
        ] },
        React.createElement(Descriptions, { layout: "horizontal", bordered: true },
            React.createElement(Descriptions.Item, { label: "Product" }, "Cloud Database"),
            React.createElement(Descriptions.Item, { label: "Billing" }, "Prepaid"),
            React.createElement(Descriptions.Item, { label: "time" }, "18:00:00"),
            React.createElement(Descriptions.Item, { label: "Amount" }, "$80.00"),
            React.createElement(Descriptions.Item, { label: "Discount" }, "$20.00"),
            React.createElement(Descriptions.Item, { label: "Official" }, "$60.00"),
            React.createElement(Descriptions.Item, { label: "Config Info" },
                "Data disk type: MongoDB",
                React.createElement("br", null),
                "Database version: 3.4",
                React.createElement("br", null),
                "Package: dds.mongo.mid",
                React.createElement("br", null),
                "Storage space: 10 GB",
                React.createElement("br", null),
                "Replication factor: 3",
                React.createElement("br", null),
                "Region: East China 1",
                React.createElement("br", null)))));
}
function DetailRowModal(Option) {
    var _a = useState(false), isEdit = _a[0], setEdit = _a[1];
    return (React.createElement(React.Fragment, null, isEdit ? React.createElement(EditRow, { edit: setEdit }) : React.createElement(ViewRow, { edit: setEdit })));
}
export default DetailRowModal;
//# sourceMappingURL=DetailRowModal.js.map