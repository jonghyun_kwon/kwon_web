import React from 'react';
import { Breadcrumb } from "antd";
function BreadContentPath() {
    //TODO: 현재 패스로 작업 필요.
    return (React.createElement(Breadcrumb, null,
        React.createElement(Breadcrumb.Item, null, "Home"),
        React.createElement(Breadcrumb.Item, null,
            React.createElement("a", { href: "" }, "Application Center")),
        React.createElement(Breadcrumb.Item, null,
            React.createElement("a", { href: "" }, "Application List")),
        React.createElement(Breadcrumb.Item, null, "An Application")));
}
export default BreadContentPath;
//# sourceMappingURL=BreadContentPath.js.map