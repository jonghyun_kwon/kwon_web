import React from 'react';
function Home() {
    return (React.createElement("div", { className: "max-w-sm mx-auto flex p-6 bg-white rounded-lg shadow-xl" },
        React.createElement("div", { className: "flex-shrink-0" },
            React.createElement("img", { className: "h-12 w-12", src: "/img/logo.svg", alt: "ChitChat Logo" })),
        React.createElement("div", { className: "ml-6 pt-1" },
            React.createElement("h4", { className: "text-xl text-gray-900 leading-tight" }, "HOME"),
            React.createElement("p", { className: "text-base text-gray-600 leading-normal" }, "You have a new message!"),
            React.createElement("div", { className: "bg-blue-100" }, "blue100"),
            React.createElement("div", { className: "bg-blue-300" }, "blue200"),
            React.createElement("div", { className: "bg-blue-500" }, "blue500"),
            React.createElement("div", { className: "bg-blue-700" }, "blue700"))));
}
export default Home;
//# sourceMappingURL=Home.js.map