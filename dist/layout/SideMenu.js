import React from 'react';
import { Layout, Menu } from 'antd';
import { UserOutlined, DollarOutlined, HomeOutlined, NotificationOutlined, SettingOutlined, } from '@ant-design/icons';
import { Link } from 'react-router-dom';
import SubMenu from "antd/es/menu/SubMenu";
var Sider = Layout.Sider;
var menuData = [
    {
        key: 'member_mgr',
        icon: React.createElement(UserOutlined, null),
        title: '핀다회원관리',
        href: '/member_mgr',
        child: [
            {
                key: 'member_list',
                icon: '',
                title: '회원목록',
                href: '/member_list',
            },
            {
                key: 'notice_list',
                icon: '',
                title: '공지사항',
                href: '/notice_list',
            },
            {
                key: 'terms_and_conditions',
                icon: '',
                title: '약관',
                href: '/terms_and_conditions',
            },
            {
                key: 'member_notification',
                icon: '',
                title: '한줄알림',
                href: '/member_notification',
            },
            {
                key: 'member_notification_setting',
                icon: '',
                title: '앱 알림설정',
                href: '/member_notification_setting',
            }
        ]
    },
    {
        key: 'loan_apply_mgr',
        icon: React.createElement(DollarOutlined, null),
        title: '대출신청관리',
        href: '/loan_apply_mgr',
        child: [
            {
                key: 'loan_apply_bank_list',
                icon: '',
                title: '금융기관',
                href: '/loan_apply_bank_list',
            },
            {
                key: 'loan_apply_product_list',
                icon: '',
                title: '상품',
                href: '/loan_apply_product_list',
            },
            {
                key: 'loan_apply_review_list',
                icon: '',
                title: '후기',
                href: '/loan_apply_review_list',
            },
            {
                key: 'loan_apply_preopen_bank_list',
                icon: '',
                title: '가 오픈 금융기관',
                href: '/loan_apply_preopen_bank_list',
            }
        ]
    }, {
        key: 'loan_apply_hr_mgr',
        icon: React.createElement(HomeOutlined, null),
        title: '전세관리',
        href: '/loan_apply_hr_mgr',
        child: [
            {
                key: 'loan_apply_hr_product_list',
                icon: '',
                title: '상품',
                href: '/loan_apply_hr_product_list',
            }
        ]
    }, {
        key: 'notification_mgr',
        icon: React.createElement(NotificationOutlined, null),
        title: '알림관리',
        href: '/notification_mgr',
        child: [
            {
                key: 'notification_plan_list',
                icon: '',
                title: '알림 계획',
                href: '/notification_plan_list',
            },
            {
                key: 'user_segment',
                icon: '',
                title: '멤버 분류',
                href: '/user_segment',
            }
        ]
    }, {
        key: 'administrator_mgr',
        icon: React.createElement(SettingOutlined, null),
        title: '관리 메뉴',
        href: '/administrator_mgr',
        child: [
            {
                key: 'administrator_audit_log',
                icon: '',
                title: '감시 로그',
                href: '/administrator_audit_log',
            },
            {
                key: 'administrator_user_list',
                icon: '',
                title: '관리자 관리',
                href: '/administrator_user_list',
            },
            {
                key: 'administrator_role_list',
                icon: '',
                title: '권한관리',
                href: '/administrator_role_list',
            }
        ]
    },
];
function SideMenu(Options) {
    var menu = menuData.map(function (submenu) {
        return (React.createElement(SubMenu, { key: submenu.key, title: submenu.title, icon: submenu.icon }, submenu.child.map(function (menu) {
            return (React.createElement(Menu.Item, { key: menu.key },
                React.createElement(Link, { to: menu.href }, menu.title)));
        })));
    });
    return (React.createElement(Sider
    // trigger={null} collapsible
    , { 
        // trigger={null} collapsible
        collapsed: Options.collapsed, breakpoint: "lg", 
        // collapsedWidth="0"
        onBreakpoint: function (broken) {
            console.log(broken);
        }, onCollapse: function (collapsed, type) {
            console.log(collapsed, type);
        }, className: "h-full" },
        React.createElement("div", { className: "logo" }),
        React.createElement(Menu, { theme: "dark", mode: "inline", defaultSelectedKeys: ['loan_apply_bank_list'] }, menu)));
}
export { SideMenu, menuData };
//# sourceMappingURL=SideMenu.js.map