import { useState, useEffect } from 'react';
function useMenuCollapsedStatus() {
    var _a = useState(false), isCollapsed = _a[0], setIsCollapsed = _a[1];
    useEffect(function () {
        console.log("effect:" + isCollapsed);
    });
    function toggle() {
        console.log("now isCollapsed : " + isCollapsed);
        setIsCollapsed(!isCollapsed);
        console.log("changed isCollapsed : " + isCollapsed);
    }
    return [isCollapsed, toggle];
}
export default useMenuCollapsedStatus;
//# sourceMappingURL=useMenuCollapsed.js.map