import "./MainLayout.sss";
import React from 'react';
import { Layout } from 'antd';
import { MenuUnfoldOutlined, MenuFoldOutlined, } from '@ant-design/icons';
var Header = Layout.Header, Content = Layout.Content;
import { Route, Switch } from "react-router-dom";
import Home from "../pages/Home";
import About from "../pages/About";
import LoanApplyBankList from "../pages/LoanApplyBankList";
import useMenuCollapsedStatus from "./useMenuCollapsed";
import { SideMenu } from "./SideMenu";
function MainLayout() {
    var _a = useMenuCollapsedStatus(), isCollapsed = _a[0], toggle = _a[1];
    return (React.createElement(Layout, { className: "h-screen" },
        React.createElement(SideMenu, { collapsed: isCollapsed }),
        React.createElement(Layout, { className: "h-full" },
            React.createElement(Header, { className: "site-layout", style: { padding: 0 } }, isCollapsed ?
                React.createElement(MenuUnfoldOutlined, { className: 'trigger', onClick: function () { return toggle(); } })
                : React.createElement(MenuFoldOutlined, { className: 'trigger', onClick: function () { return toggle(); } })),
            React.createElement(Content, { className: "mt-1" },
                React.createElement("div", null,
                    React.createElement(Switch, null,
                        React.createElement(Route, { path: "/home", component: Home }),
                        React.createElement(Route, { path: "/about", component: About }),
                        React.createElement(Route, { path: "/loan_apply_bank_list", component: LoanApplyBankList })))))));
}
export default MainLayout;
//# sourceMappingURL=MainLayout.js.map