import 'assets/styles/main.less';
import "assets/styles/tailwind.sss";
import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter, Route } from "react-router-dom";
import MainLayout from "./layout/MainLayout";
var Root = function () {
    return (React.createElement("div", { className: "App" },
        React.createElement(BrowserRouter, null,
            React.createElement(Route, { path: "/", component: MainLayout }))));
};
ReactDOM.render(React.createElement(Root, null), document.getElementById('root'));
//# sourceMappingURL=App.js.map