const plugin = require('tailwindcss/plugin')

const antColor = require('@ant-design/colors');



let { generate, presetPrimaryColors } = antColor;


let colorObject = {
    black: {
        '100': '#303030',
        '200': '#2b2b2b',
        '300': '#404040'
    }
};
Object.keys(presetPrimaryColors).forEach(function (key) {
        colorObject[key] = {};
        let colorArray = generate(key, {theme: 'dark'})
        for (let i = 0; i < colorArray.length; i++) {
            colorObject[key][i + '00'] = colorArray[i];
        }
    }
);

module.exports = {
    // future: {
    //     defaultLineHeights: true
    // },
    theme: {
        screens: {
            sm: '640px',
            md: '768px',
            lg: '1024px',
            xl: '1280px',
        },
        // fontFamily: {
        //     display: ['Gilroy', 'sans-serif'],
        //     body: ['Graphik', 'sans-serif'],
        // },
        // borderWidth: {
        //     default: '1px',
        //     '0': '0',
        //     '2': '2px',
        //     '4': '4px',
        // },
        extend: {
            colors: colorObject,
            // spacing: {
            //     '96': '24rem',
            //     '128': '32rem',
            // }
            spacing: {
                tab: '59px',
                gnb: '56px',
                'float-safety': '12rem',
                '05': '0.125rem',
                '3/2': '0.375rem',
                '5/2': '0.625rem', // 10px
                7: '1.75rem',
                9: '2.25rem', // 36px
                11: '2.75rem',
                14: '3.5rem',
                13: '3.25rem',
                15: '3.75rem',
                17: '4.25rem',
                18: '4.5rem',
                22: '5.5rem',
                40: '10rem',
                'qna-margin': '3rem',
                'form-margin': '40px',
                xs: '0.125rem',
                sm: '1.25rem',
                'pc-grid-p': '0.625rem',
                'input-mb-no-label': '2.5rem',
                'input-mb': '59px',
                'input-pc-no-label': '3rem',
                'input-pc': '71px',
            },
            inset: {
                '025': '1px',
                '05': '0.125rem',
                '075': '3px',
                1: '0.25rem',
                2: '0.5rem',
                3: '0.75rem',
                4: '1rem',
                5: '1.25rem',
                6: '1.5rem',
                7: '1.75rem',
                8: '2rem',
                9: '2.25rem',
                10: '2.5rem',
                12: '3rem',
                14: '3.5rem',
                16: '4rem',
                18: '4.5rem',
                19: '4.75rem',
                '-025': '-1px',
                '-05': '-0.125rem',
                '-1': '-0.25rem',
                '-2': '-0.5rem',
                '-3': '-0.75rem',
                '-4': '-1rem',
                '-5': '-1.25rem',
                '-6': '-1.5rem',
                '-7': '-1.75rem',
                '-8': '-2rem',
                '-9': '-2.25rem',
                '-10': '-2.5rem',
                '-16': '-4rem',
                '-18': '-4.5rem',
                '-36': '-9rem',
            },
        }
    },
    plugins: [
        plugin(function({ addBase, config }) {
            addBase({
                'h1': { fontSize: config('theme.fontSize.2xl') },
                'h2': { fontSize: config('theme.fontSize.xl') },
                'h3': { fontSize: config('theme.fontSize.lg') },
            })
        })
    ]
}